from django import forms
class welcome(forms.Form):
    name = forms.CharField(label= 'Name')
    language = forms.ChoiceField(choices=[('&to=en','English'),('&to=de','German'),('&to=it','Italian'),('&to=zh-Hans','Chinese'),('&to=fr','French'),('&to=tr','Turkish'),('&to=es','Spanish')])

class firstUser(forms.Form):
    destination = forms.CharField(label='Destination')
    result = forms.CharField(label='Result')
    translateTo = forms.ChoiceField(choices=[('&to=en','English'),('&to=de','German'),('&to=it','Italian'),('&to=zh-Hans','Chinese'),('&to=fr','French'),('&to=tr','Turkish'),('&to=es','Spanish')])

class TranslateForm(forms.Form):
    content = forms.CharField(label = 'Location')
    translateTo = forms.ChoiceField(choices=[('&to=en','English'),('&to=de','German'),('&to=it','Italian'),('&to=zh-Hans','Chinese'),('&to=fr','French'),('&to=tr','Turkish'),('&to=es','Spanish')])

class errorMessage(forms.Form):
    contentError = forms.CharField(label='Location')
    translateToError = forms.ChoiceField(choices=[('&to=en', 'English'), ('&to=de', 'German'), ('&to=it', 'Italian'), ('&to=zh-Hans', 'Chinese'),('&to=fr','French'),('&to=tr','Turkish'),('&to=es','Spanish')])