from django.urls import path
from . import views

app_name = 'mainpage'

urlpatterns = [
    path('main/',views.welcomePage,name='welcomePage'),
    path('main/firstUser/',views.firstUserInfo,name='firstUser'),
    path('main/firstUser/secondUser/',views.obtainResult,name='obtainResult')
]