from django.shortcuts import render
from django.http import HttpResponse
from .form import TranslateForm,errorMessage,firstUser,welcome
import http.client
import json
import urllib.parse
import uuid

subscriptionKey = '115c1c16339a4b368efbf785f4e74a40'
host = 'api.cognitive.microsofttranslator.com'
path = '/translate?api-version=3.0'
content1 = "Welcome to use our translator! In the first input box, you need to type a location. In second input box, you should choose a language which other people can understand. We provide three buttons to specify a trip mode."

def parseTextToJson(text):
    requestBody = [{
        'Text': text,
    }]
    content = json.dumps(requestBody).encode('utf-8')
    return content

def translate(text,translateTo):
    content = parseTextToJson(text)
    params = str(translateTo)
    headers = {
        'Ocp-Apim-Subscription-Key': subscriptionKey,
        'Content-type': 'application/json',
        'X-ClientTraceId': str(uuid.uuid4())
    }
    conn = http.client.HTTPSConnection(host)
    conn.request("POST", path + params, content, headers)
    response = conn.getresponse()
    result = response.read()
    output_str = json.dumps(json.loads(result), indent=4, ensure_ascii=False)
    output_dict = json.loads(result)[0]
    translateResult = output_dict["translations"][0]["text"]
    return translateResult

def obtainResult(request):
    if request.method =='POST':
        formResult = TranslateForm(request.POST)
        errorResult = errorMessage(request.POST)
        if formResult.is_valid():
            form = TranslateForm(
                initial={'translateTo': str(formResult.cleaned_data['translateTo']),'content':str(formResult.cleaned_data['content'])}
            )
            form2 = errorMessage(
                initial={'translateToError': '&to=en','contentError':'Sorry I don`t understand'}
            )
            content = formResult.cleaned_data['content']
            translateResult = translate(content,formResult.cleaned_data['translateTo'])
            return render(request, 'webPage/mainPage.html', context={'form': form,'form2':form2,'last_result': translateResult})
        else:
            if errorResult.is_valid():
                translateResult = translate(errorResult.cleaned_data['contentError'], errorResult.cleaned_data['translateToError'])
                form = TranslateForm(
                    initial={'translateTo': '&to=en', 'content': 'Hi'}
                )
                form2 = errorMessage(
                    initial={'translateToError': str(errorResult.cleaned_data['translateToError']),'contentError':errorResult.cleaned_data['contentError']}
                )
                return render(request, 'webPage/mainPage.html', context={'form': form,'form2':form2,'errorResult':translateResult})
    else:
        form = TranslateForm(
            initial={'translateTo': '&to=en','content':'Hi'}
        )
        form2 = errorMessage(
            initial={'translateToError': '&to=en','contentError':'Sorry, I don`t understand'}
        )
        return render(request, 'webPage/mainPage.html', context={'form': form,'form2':form2})

def firstUserInfo(request):
    if request.method =='POST' :
        obtainResult = firstUser(request.POST)
        welcomePage = welcome(request.POST)
        if welcomePage.is_valid():
            content = content1
            translateResult = translate(content, welcomePage.cleaned_data['language'])
            sayHello = 'Hi，' + welcomePage.cleaned_data['name'] + '！Thank you for using our App.'
            form = firstUser(
                initial={'destination': 'Duke University', 'translateTo': '&to=en','result':'*'}
            )
            return render(request, 'webPage/firstUser.html', context={'form': form,'content': translateResult,'sayHello': sayHello})
        else:
            if obtainResult.is_valid():

                if 'byBus' in request.POST:
                    content = "Hi, I want to go to " + str(obtainResult.cleaned_data['destination']) + " by bus. How can I get there?"
                else:
                    if 'onFoot' in request.POST:
                        content = "Hi, I want to go to " + str(
                            obtainResult.cleaned_data['destination']) + " on foot. How can I get there?"
                    else:
                        content = "Hi, I want to go to " + str(
                            obtainResult.cleaned_data['destination']) + " by car. How can I get there?"
                translateResult = translate(content,obtainResult.cleaned_data['translateTo'])
                form = firstUser(
                    initial={'destination': obtainResult.cleaned_data['destination'],
                             'translateTo': obtainResult.cleaned_data['translateTo'], 'result': translateResult}
                )
                return render(request, 'webPage/firstUser.html', context={'form': form,'content': content1})
            else:
                return HttpResponse("Form is invalid")
    else:
        form = firstUser(
                initial={'destination': 'Duke University', 'translateTo':'&to=en','result':'*'}
            )
        return render(request, 'webPage/firstUser.html', context={'form': form,'content': content1})

def welcomePage(request):
    form = welcome()
    return render(request,'webPage/welcomePage.html',context={'form':form})